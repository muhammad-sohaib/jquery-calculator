### __ __ Project name
#####**Simple JQuery Calculator**
###Description
**This project made in JQuery. Basically ,I make simple calculator in JQuery which performs the basic operation such as addition , subtraction, multiplication and division. I take two input fields and result stored in third field**
### Installation
-**Install the Apache tool.**
-**Download JQuery libraries.**
### Features
#####**It performs the basic operation such as addition, subtraction, multiplication and division.**
### Languages
-**JQuery.**
-**CSS.**
-**HTML**
###Links
[ ]**https://jsfiddle.net/javedfiddle/s4j7vm37/**
### Author
##### M.Sohaib
### Copyright
#####**IT Serve Group @ 2018.All Rights Reserved.**
### Reference
#####**IT Serve Group**

